package br.com.tecnisys.pipeline;

import static org.assertj.core.api.Assertions.assertThat;


import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class PipelineApplicationTests {

	@Autowired
	private SomaEndpoint somaEndpoint;

	@Test
	void contextLoads() {
		assertThat(somaEndpoint).isNotNull();
		Resultado resultado = somaEndpoint.somar(1.0,2.0);
		assertThat(resultado.getSoma()).isEqualTo(3.0);
	}



}
